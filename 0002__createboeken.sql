use ModernWays;
create table boeken(
voornaam varchar(50) charset utf8mb4,
naam varchar(50) charset utf8mb4,
titel varchar(255) charset utf8mb4,
stad varchar(50) charset utf8mb4,
verschijningsjaar varchar(4) charset utf8mb4,
-- alleen het jaar
uitgeverij varchar(80) charset utf8mb4,
herdruk varchar(4) charset utf8mb4,
commentaar varchar(1000) charset utf8mb4
);